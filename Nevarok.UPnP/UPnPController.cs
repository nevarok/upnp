﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Net.Sockets;
using System.Text;
using System.Xml;
using System.Collections.Generic;

namespace Nevarok.UPnP {
	public class UPnPController {
		public enum Protocol {
			TCP,
			UDP
		}

		private const string SOAP_ADD_PORT_MAPPING =
			"<?xml version=\"1.0\"?>" +
			"<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
			"<s:Body>" +
			"<u:AddPortMapping xmlns:u=\"urn:schemas-upnp-org:service:WANIPConnection:1\">" +
			"<NewRemoteHost></NewRemoteHost>" +
			"<NewExternalPort>{0}</NewExternalPort>" +
			"<NewProtocol>{1}</NewProtocol>" +
			"<NewInternalPort>{2}</NewInternalPort>" +
			"<NewInternalClient>{3}</NewInternalClient>" +
			"<NewEnabled>{4}</NewEnabled>" +
			"<NewPortMappingDescription>{5}</NewPortMappingDescription>" +
			"<NewLeaseDuration>{6}</NewLeaseDuration>" +
			"</u:AddPortMapping>" +
			"</s:Body>" +
			"</s:Envelope>";

		private const string SOAP_DELETE_PORT_MAPPING =
			"<?xml version=\"1.0\"?>" +
			"<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
			"<s:Body>" +
			"<u:DeletePortMapping xmlns:u=\"urn:schemas-upnp-org:service:WANIPConnection:1\">" +
			"<NewRemoteHost></NewRemoteHost>" +
			"<NewExternalPort>{0}</NewExternalPort>" +
			"<NewProtocol>{1}</NewProtocol>" +
			"</u:DeletePortMapping>" +
			"</s:Body>" +
			"</s:Envelope>";

		private const string M_SEARCH =
			"M-SEARCH * HTTP/1.1\r\n" +
			"HOST:239.255.255.250:1900\r\n" +
			"MAN:\"ssdp:discover\"\r\n" +
			"ST:upnp:rootdevice\r\n" +
			"MX:3\r\n\r\n";

		private static string descriptionURL { get; set; }
		private static string routerAddress { get; set; }
		private static string controlURL { get; set; }

		private static XmlDocument descriptionXML { get; set; }

		public static IPAddress localIP { get; private set; }

		public UPnPController() { }

		/// <summary>
		/// Discover router.
		/// </summary>
		/// <param name="maxTries">Max tries.</param>
		public static bool Discover(int maxTries) {
			IPEndPoint MulticastEndPoint = new IPEndPoint(IPAddress.Parse("239.255.255.250"), 1900);

			localIP = GetLocalAddress();

			IPEndPoint LocalEndPoint = new IPEndPoint(localIP, 0);

			using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)) {
				socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

				socket.Bind(LocalEndPoint);

				socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(MulticastEndPoint.Address, IPAddress.Any));
				socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 2);
				socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastLoopback, true);

				byte[] data = Encoding.UTF8.GetBytes(M_SEARCH);

				socket.SendTo(data, data.Length, SocketFlags.None, MulticastEndPoint);
				socket.SendTo(data, data.Length, SocketFlags.None, MulticastEndPoint);
				socket.SendTo(data, data.Length, SocketFlags.None, MulticastEndPoint);

				byte[] ReceiveBuffer = new byte[64000];

				int receivedBytes = 0;

				while (maxTries > 0) {
					if (socket.Available > 0) {
						receivedBytes = socket.Receive(ReceiveBuffer, SocketFlags.None);

						if (receivedBytes > 0) {
							string response = Encoding.UTF8.GetString(ReceiveBuffer, 0, receivedBytes);

							Console.WriteLine(response);
							if (IsResponseOK(response)) {
								try {
									descriptionURL = GetLocationURL(response);

									string description = Get(descriptionURL);

									if (CheckDescription(description)) {
										descriptionXML = GetDescriptionXML(description);
										routerAddress = GetBaseURL(descriptionXML);

										controlURL = string.Format("{0}{1}", routerAddress, GetControlURL(descriptionXML));

										return true;
									}
								} catch (Exception e) {
									Console.WriteLine(e.ToString());
									return false;
								}
							}
						}
					} else {
						maxTries--;
						Thread.Sleep(100);
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Forewords port.
		/// </summary>
		/// <param name="externalPort">External port.</param>
		/// <param name="protocol">Protocol.</param>
		/// <param name="internalPort">Internal port.</param>
		/// <param name="localIP">Local ip.</param>
		/// <param name="isEnabled">If set to <c>true</c> is enabled.</param>
		/// <param name="description">Description.</param>
		/// <param name="duration">Duration.</param>
		public static void AddPortMapping(int externalPort,
										  Protocol protocol,
										  int internalPort,
										  IPAddress localIP,
										  bool isEnabled,
										  string description,
										  int duration = 0) {

			string data = string.Format(SOAP_ADD_PORT_MAPPING,
										//string.Empty,
										externalPort,
										protocol,
										internalPort,
										localIP,
										isEnabled ? 1 : 0,
										description,
										duration);
			Post(controlURL, data, "AddPortMapping");
			//Console.WriteLine(Post(controlURL, data));
		}

		public static void DeletePortMapping(int externalPort,
										  Protocol protocol) {

			string data = string.Format(SOAP_DELETE_PORT_MAPPING,
										//string.Empty,
										externalPort,
										protocol);

			Post(controlURL, data, "DeletePortMapping");
		}

		private static string Get(string url) {
			using (WebClient client = new WebClient()) {
				return client.DownloadString(url);
			}
		}

		private static string Post(string url, string data, string function) {
			using (var client = new WebClient()) {
				client.Headers = new WebHeaderCollection();

				client.Headers.Add("SOAPACTION", string.Format("\"urn:schemas-upnp-org:service:WANIPConnection:1#{0}\"", function));

				return client.UploadString(url, data);
			}
		}

		private static bool CheckDescription(string text) {
			return text.Contains("WANIPConnection:1");
		}

		private static XmlDocument GetDescriptionXML(string text) {
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.LoadXml(text);

			return xmlDocument;
		}

		private static string GetBaseURL(XmlDocument xmlDocument) {
			XmlNode rootNode = xmlDocument.DocumentElement;

			if (rootNode.HasChildNodes) {
				string baseURL = GetChildNode(rootNode, "URLBase").InnerText;
				baseURL = baseURL.Remove(baseURL.Length - 1);
				return baseURL;
			}

			return string.Empty;
		}

		private static string GetControlURL(XmlDocument xmlDocument) {
			XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDocument.NameTable);

			nsManager.AddNamespace("tns", "urn:schemas-upnp-org:device-1-0");

			XmlNode controlNode = xmlDocument.SelectSingleNode("//tns:service[tns:serviceType=\"urn:schemas-upnp-org:service:WANIPConnection:1\"]/tns:controlURL/text()", nsManager);
			//			XmlNode eventNode = xmlDocument.SelectSingleNode("//tns:service[tns:serviceType=\"urn:schemas-upnp-org:service:WANIPConnection:1\"]/tns:eventSubURL/text()", nsManager);

			return controlNode.Value;
		}

		private static List<XmlNode> GetChildNodes(XmlNode fromNode, string name) {
			List<XmlNode> childNodes = new List<XmlNode>();

			for (int i = 0; i < fromNode.ChildNodes.Count; i++) {
				XmlNode childNode = fromNode.ChildNodes[i];

				if (childNode.Name == name) {
					childNodes.Add(childNode);
				}
			}

			return childNodes;
		}

		private static XmlNode GetChildNode(XmlNode fromNode, string name) {
			for (int i = 0; i < fromNode.ChildNodes.Count; i++) {
				XmlNode childNode = fromNode.ChildNodes[i];

				if (childNode.Name == name) {
					return childNode;
				}
			}

			return null;
		}

		private static string GetRouterAddress(string text) {
			int lastIndex = text.LastIndexOf('/');
			int length = text.Length - lastIndex;
			text = text.Remove(lastIndex, length);

			return text;
		}

		private static string GetLocationURL(string text) {
			string openTag = "location: ";
			string closeTag = "\r";

			int index = text.IndexOf(openTag, StringComparison.OrdinalIgnoreCase) + openTag.Length;
			text = text.Remove(0, index);

			index = text.IndexOf(closeTag, StringComparison.OrdinalIgnoreCase);
			text = text.Remove(index);

			return text;
		}

		private static bool IsResponseOK(string text) {
			return text.Contains("HTTP/1.1 200 OK");
		}

		public static IPAddress GetLocalAddress() {
			NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

			foreach (NetworkInterface network in networkInterfaces) {

				IPInterfaceProperties properties = network.GetIPProperties();

				if (properties.GatewayAddresses.Count == 0)
					continue;

				foreach (IPAddressInformation address in properties.UnicastAddresses) {

					if (address.Address.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork)
						continue;

					if (IPAddress.IsLoopback(address.Address))
						continue;

					return address.Address;
				}
			}
			return default(IPAddress);
		}
	}
}

